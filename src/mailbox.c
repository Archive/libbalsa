#include "libbalsa.h"

/*
 * allocate a new mailbox
 */
Mailbox *
mailbox_new (MailboxType type)
{
  Mailbox *mailbox;

  switch (type)
    {
    case MAILBOX_MBOX:
    case MAILBOX_MH:
    case MAILBOX_MAILDIR:
      mailbox = (Mailbox *) g_malloc (sizeof (MailboxLocal));
      MAILBOX_LOCAL (mailbox)->path = NULL;
      break;

    case MAILBOX_IMAP:
      mailbox = (Mailbox *) g_malloc (sizeof (MailboxIMAP));
      MAILBOX_IMAP (mailbox)->path = NULL;
      break;

    default:
      return NULL;
    }

  mailbox->type = type;
  mailbox->name = NULL;
  mailbox->ref_count = 0;

  mailbox->lock = FALSE;

  mailbox->messages = 0;
  mailbox->new_messages = 0;
  mailbox->message_list = NULL;

  return mailbox;
}


void
mailbox_free (Mailbox * mailbox)
{
  if (!mailbox)
    return;

  while (mailbox->ref_count > 0)
    mailbox_unref (mailbox);

  g_free (mailbox->name);

  switch (mailbox->type)
    {
    case MAILBOX_MBOX:
    case MAILBOX_MH:
    case MAILBOX_MAILDIR:
      g_free (MAILBOX_LOCAL (mailbox)->path);
      break;

    case MAILBOX_IMAP:
      g_free (MAILBOX_IMAP (mailbox)->path);
      break;

    case MAILBOX_UNKNOWN:
      break;
    }

  /* TODO remove message list */
  
  g_free (mailbox);
  mailbox = NULL;
}

void
mailbox_ref (Mailbox * mailbox)
{
  mailbox->ref_count++;
}

void
mailbox_open_unref (Mailbox * mailbox)
{
  mailbox->ref_count--;
  if (mailbox->ref_count == 0)
  {
    mailbox_free(mailbox);
    mailbox = NULL;
  }
}

