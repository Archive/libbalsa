#include <glib.h>

#ifndef __LIBBALSA_H__
#define __LIBBALSA_H__

/*
 * public macros
 */
#define MAILBOX(mailbox)        ((Mailbox *)(mailbox))
#define MAILBOX_LOCAL(mailbox)  ((MailboxLocal *)(mailbox))

#define MAILBOX_IMAP(mailbox)   ((MailboxIMAP *)(mailbox))

/*
 * enumes
 */
typedef enum
  {
    MAILBOX_MBOX,
    MAILBOX_MH,
    MAILBOX_MAILDIR,
    MAILBOX_IMAP,
    MAILBOX_UNKNOWN
  }
MailboxType;

typedef enum
  {
    SERVER_POP3,
    SERVER_IMAP,
    SERVER_UNKNOWN
  }
ServerType;

/*
 * strucutres
 */
typedef struct _Header Header;

typedef struct _Mailbox Mailbox;
typedef struct _MailboxLocal MailboxLocal;

typedef struct _Server Server;
typedef struct _ServerPOP3 ServerPOP3;
typedef struct _ServerIMAP ServerIMAP;

typedef struct _MailboxWatcherMessage MailboxWatcherMessage;
typedef struct _MailboxWatcherMessageNew MailboxWatcherMessageNew;

typedef struct _Message Message;
typedef struct _Address Address;
typedef struct _Body Body;

struct _Mailbox
  {
    MailboxType type;
    gchar *name;
    guint ref_count;

    gboolean lock;		/* TRUE if the mailbox is being accessed directly */

    glong messages;
    glong new_messages;
    GList *message_list;
  };

/* mbox, mh, maildir, etc... */
struct _MailboxLocal
  {
    Mailbox mailbox;
    gchar *path;
  };

struct _Server
  {
    ServerType type;
    gchar *name;
    guint ref_count;

    gchar *server;
    gchar *user;
    gchar *passwd;
    gint port;
  };

struct _ServerPOP3
  {
    Server server;
    gboolean check;		/* if FALSE, do not check this mailbox */
    gboolean remove;		/* If TRUE, remove from server */
  };

struct _ServerIMAP
  {
    Server server;
    GList *mailboxes;		/* GList of MailboxIMAP's */
  };

struct _MailboxIMAP
  {
    Mailbox mailbox;

    ServerIMAP *server;
    gchar *path;
    gint port;
  };


struct _Message
  {
    /* the mailbox this message belongs to */
    Mailbox *mailbox;

    /* list containing Header structures */
    GList *headers;

    /* flags */
    MessageFlags flags;

    /* the ordered numberic index of this message in 
     * the mailbox beginning from 1, not 0 */
    glong msgno;

    /* message composition date string */
    time_t datet;


/*--* Frequlently accessed headers *--*/
    /* from, sender, and reply addresses */
    Address *from;
    Address *sender;
    Address *reply_to;
    gchar *subject;
    GList *to_list;
    GList *cc_list;
    GList *bcc_list;

    /* message body */
    guint body_ref;
    GList *body_list;
  };

struct _Header
  {
    gchar *name;		/* Header name WITH the : (i think...?) */
    gchar *value;		/* Header value */
  };

struct _Address
  {
    gchar *personal;		/* full text name */
    gchar *mailbox;		/* user name and host (mailbox name) on remote system */
  };


struct _Body
  {
    /* this needs to be redone */
  };

/* initialize libbalsa */
void libbalsa_init (void);


/* 
 * refcounting for mailboxes
 */
void mailbox_ref (Mailbox * mailbox);
void mailbox_unref (Mailbox * mailbox);

/*
 * create and destroy a mailbox structure
 */
Mailbox *mailbox_new (MailboxType type);
void mailbox_free (Mailbox * mailbox);

/*
 * mailbox util functions
 */
gint mailbox_new_messages (Mailbox *);


/*
 * messages
 */
Message *message_new (void);
void message_free (Message * message);

void message_copy (Message * message, Mailbox * dest);
void message_move (Message * message, Mailbox * mailbox);

/*
 * addresses
 */
Address *address_new (void);
void address_free (Address * address);


/*
 * body
 */
Body *body_new (void);
void body_free (Body * body);


/*
 * misc mailbox releated functions
 */
MailboxType get_mailbox_type (gchar * description);
gchar *get_mailbox_description (MailboxType type);

/* check the mailboxes validity */
MailboxType mailbox_valid (gchar * filename);

#endif /* __LIBBALSA_H__ */
