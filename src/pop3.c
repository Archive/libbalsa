#include "libbalsa.h"

#define LOCK_MAILBOX(mailbox)\
do {\
  if (mailbox->lock)\
    {\
      g_print (_("*** ERROR: Mailbox Lock Exists: %s ***\n"), __PRETTY_FUNCTION__);\
      return;\
    }\
  else\
    mailbox->lock = TRUE;\
} while (0)

#define UNLOCK_MAILBOX(mailbox)          mailbox->lock = FALSE;

static gboolean 
real_open_mailbox (MailboxPOP3 * mailbox)
{
  /* connect, etc etc etc.. retreive messages.. *shrug* */
  return FALSE;
}

gboolean 
open_pop3_mailbox (MailboxPOP3 * mailbox)
{
  LOCK_MAILBOX (mailbox);
  mailbox_ref (mailbox);
  return real_open_mailbox;
}
